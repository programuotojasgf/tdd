using FluentAssertions;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;
using Xunit;

namespace StringCalculator.UnitTests
{
    public class CalculatorTests
    {
        [Theory]
        [AutoMockData]
        public void GivenEmpty_WhenAddIsCalled_ReturnZero(Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(string.Empty);

            // Assert
            actualResult.Should().Be(0);
        }

        [Theory]
        [InlineAutoMockData("39", 39)]
        [InlineAutoMockData("5", 5)]
        public void GivenSingleNumber_WhenAddIsCalled_ReturnConvertedResult(
            string numbers,
            int expectedResult,
            Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(numbers);

            // Assert
            actualResult.Should().Be(expectedResult);
        }

        [Theory]
        [InlineAutoMockData("2,5", 7)]
        [InlineAutoMockData("3,7", 10)]
        public void GivenTwoCommaSeparatedNumbers_WhenAddIsCalled_ReturnSum(
            string numbers,
            int expectedResult,
            Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(numbers);

            // Assert
            actualResult.Should().Be(expectedResult);
        }

        [Theory]
        [InlineAutoMockData("2,5,43,8", 58)]
        [InlineAutoMockData("3,7,9", 19)]
        public void GivenMultipleCommaSeparatedNumbers_WhenAddIsCalled_ReturnSum(
            string numbers,
            int expectedResult,
            Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(numbers);

            // Assert
            actualResult.Should().Be(expectedResult);
        }

        [Theory]
        [InlineAutoMockData("2,4|33|8,1", 48)]
        [InlineAutoMockData("3,7|9", 19)]
        public void GivenMultipleCommaOrPipeSeparatedNumbers_WhenAddIsCalled_ReturnSum(
            string numbers,
            int expectedResult,
            Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(numbers);

            // Assert
            actualResult.Should().Be(expectedResult);
        }

        [Theory]
        [InlineAutoMockData(":2,4|33:8", 47)]
        [InlineAutoMockData("?3,7|9?1", 20)]
        public void GivenAdditionalSeparator_WhenAddIsCalled_ReturnSum(
            string numbers,
            int expectedResult,
            Calculator calculator)
        {
            // Arrange

            // Act
            var actualResult = calculator.Add(numbers);

            // Assert
            actualResult.Should().Be(expectedResult);
        }
    }
}