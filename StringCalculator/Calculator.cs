﻿using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class Calculator
    {
        private static readonly HashSet<char> PresetSeparators = new HashSet<char> { ',', '|' };

        public int Add(string numbers)
        {
            if (numbers == string.Empty)
            {
                return 0;
            }

            var separators = new HashSet<char>(PresetSeparators);
            if (IsFirstCharAnAdditionalSeparator(numbers))
            {
                separators.Add(numbers[0]);
                numbers = numbers.Substring(1);
            }

            return numbers.Split(separators.ToArray()).Sum(int.Parse);
        }

        private static bool IsFirstCharAnAdditionalSeparator(string numbers) => !int.TryParse(numbers[0].ToString(), out _);
    }
}